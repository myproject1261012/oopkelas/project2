package Project2;

public class Mahasiswa {
    String Nama;
    String Nim;
    MataKuliah mataKuliah;

    public void setMataKuliah (MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }
    public MataKuliah getMataKuliah () {
        return mataKuliah;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }
    public String getNama() {
        return Nama;
    }

    public void SetNim(String Nim) {
        this.Nim = Nim;
    }
    public String getNim() {
        return Nim;
    }
}
