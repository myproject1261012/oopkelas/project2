package Project2;

import java.util.ArrayList;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        ArrayList mahasiswa = new ArrayList<>();
        Scanner in = new Scanner(System.in);

        boolean nextMahasiswa = true;
        while (nextMahasiswa) {

        System.out.println("==============Pengisian Data KHS===============");
        System.out.print("Masukan Nama\t: ");
        String Nama = in.nextLine();

        System.out.print("Masukan Nim\t: ");
        String Nim = in.nextLine();
        System.out.println("----------------------------------------------");

        boolean next = true;
        while (next) {

            System.out.print("Masukan Kode Mata Kuliah: ");
            String KodeMK = in.nextLine();

            System.out.print("Masukan Nama Mata Kuliah: ");
            String NamaMK = in.nextLine();

            System.out.print("Masukan Nilai Mata Kuliah: ");
            int NilaiAngka = in.nextInt();
            in.nextLine();


            Mahasiswa mhs = new Mahasiswa();
            mhs.setNama(Nama);
            mhs.SetNim(Nim);
            
            MataKuliah matKul = new MataKuliah();
            matKul.setKodeMK(KodeMK);
            matKul.setNamaMK(NamaMK);
            matKul.setNilai(NilaiAngka);
            mhs.setMataKuliah(matKul);
            
            mahasiswa.add(mhs);
            System.out.print("tambah lagi? (y/t) ");
            String tambah = in.nextLine();
            if (tambah.equals("t")) {
                next = false;
            }
        }
        System.out.print("tambah Mahasiswa lagi? (y/t) ");
        String tambahMahasiswa = in.nextLine();
        if (tambahMahasiswa.equals("t")) {
            nextMahasiswa = false;
    }
}  

//Display Info
        for (int i = 0; i < mahasiswa.size(); i++) {
            Mahasiswa mhs = (Mahasiswa) mahasiswa.get(i); 
            System.out.println("\n================Informasi KHS=================");
            System.out.println("Nama\t: " + mhs.getNama());
            System.out.println("Nim\t: " + mhs.getNim());
            System.out.println("----------------------------------------------");
            System.out.println("|  Kode  |    Mata Kuliah   |     Nilai      |");
            System.out.println("|--------|------------------|----------------|");
            System.out.printf("|%-8s| %-17s| %-15s|\n", mhs.getMataKuliah().getKodeMK(), mhs.getMataKuliah().getNamaMK(), mhs.getMataKuliah().Nilai);
        }
    }
}

