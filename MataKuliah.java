package Project2;

public class MataKuliah {
    String KodeMK;
    String NamaMK;
    int NilaiAngka;
    String Nilai;

//Membuat Setter dan Getter
    //Kode Mata Kuliah
    public void setKodeMK(String KodeMK) {
        this.KodeMK = KodeMK;
    }
    public String getKodeMK() {
        return KodeMK;
    }

    //Nama Mata Kuliah
    public void setNamaMK(String NamaMK) {
        this.NamaMK = NamaMK;
    }
    public String getNamaMK() {
        return NamaMK;
    }

    //Nilai Angka
    public void setNilaiAngka(int NilaiAngka) {
        this.NilaiAngka = NilaiAngka;
    }
    public int getNilaiAngka() {
        return NilaiAngka;
    }

    //Nilai
    public void setNilai(int NilaiAngka) {
            if (NilaiAngka >= 80 && NilaiAngka <= 100) {
                String Nilai = "A";
                this.Nilai = Nilai;
            } else if (NilaiAngka >= 60 && NilaiAngka <= 79) {
                String Nilai = "B";
                this.Nilai = Nilai;
            } else if (NilaiAngka >= 50 && NilaiAngka <= 59) {
                String Nilai = "C";
                this.Nilai = Nilai;
            } else if (NilaiAngka >= 40 && NilaiAngka <= 49) {
                String Nilai = "D";
                this.Nilai = Nilai;
            } else if (NilaiAngka >= 0 && NilaiAngka <= 39) {
                String Nilai = "E";
                this.Nilai = Nilai;
            } else {
                String Invalid = "Nilai Invalid";
                this.Nilai = Invalid;
            }
        }        
    }

